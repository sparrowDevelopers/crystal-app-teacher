import {Injectable} from "@angular/core";
import 'rxjs/Rx';
import {AlertController} from "ionic-angular";

@Injectable()
export class AlertService {
    constructor(private alert: AlertController) {

    }

    show(subTitle?, callback?) {
        this.alert.create({
            title: "Alert",
            subTitle: subTitle || "Unable to connect to server",
            buttons: [
                {
                    text: "Ok",
                    role: "cancel",
                    handler: () => {
                        if (callback) {
                            callback();
                        } else {
                            window.location.reload();
                        }

                    }
                }
            ]
        }).present();
    }
}