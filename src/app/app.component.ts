import {Component} from '@angular/core';
import {Platform, App, Events, LoadingController} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {LoginPage} from "../pages/login/login";
import {AndroidPermissions} from "@ionic-native/android-permissions";
import {StorageService} from "../services/storage.service";
import {Network} from "@ionic-native/network";
import {NetworkService} from "../services/network.service";
import {TabsPage} from "../pages/tabs/tabs";
import {ErrorPage} from "../pages/error/error";

@Component({
    templateUrl: 'app.html',
    providers: [AndroidPermissions, StorageService, NetworkService]
})
export class MyApp {

    constructor(platform: Platform,
                statusBar: StatusBar,
                splashScreen: SplashScreen,
                permissions: AndroidPermissions,
                public store: StorageService,
                public app: App,
                public events: Events,
                public network: Network,
                public networkProvider: NetworkService,
                public loading: LoadingController) {

        let loader = this.loading.create({content: "Initializing app..."});
        loader.present();

        platform.ready().then(() => {
            statusBar.styleDefault();
            splashScreen.hide();

            loader.dismiss();

            permissions.requestPermissions([
                permissions.PERMISSION.READ_EXTERNAL_STORAGE,
                permissions.PERMISSION.WRITE_EXTERNAL_STORAGE,
                permissions.PERMISSION.INTERNET
            ]);


            this.networkProvider.initializeNetworkEvents();

            this.events.subscribe('network:offline', () => {
                this.app.getRootNav().setRoot(ErrorPage);
            });

            // Online event
            this.events.subscribe('network:online', () => {
                window.location.reload();
            });


            platform.registerBackButtonAction(() => {
                if (this.app.getActiveNav().getActive().component.name !== "HomePage") {
                    this.app.getActiveNav().pop();
                }
            });

            this.store.get("userData").then((data) => {
                if (data !== null) {
                    this.app.getActiveNav().setRoot(TabsPage);
                } else {
                    this.app.getActiveNav().setRoot(LoginPage);
                }
            }).catch(() => {
                this.app.getActiveNav().setRoot(LoginPage);
            });

        });
    }
}

