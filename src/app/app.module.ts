import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {PagesModule} from "../pages/pages.module";
import {MyApp} from './app.component';
import {IonicStorageModule} from '@ionic/storage';
import {Network} from '@ionic-native/network';
import {HttpModule} from '@angular/http';

@NgModule({
    declarations: [
        MyApp
    ],
    imports: [
        BrowserModule,
        PagesModule,
        IonicModule.forRoot(MyApp),
        IonicStorageModule.forRoot(),
        HttpModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Network,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {
}
