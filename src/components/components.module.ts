import {NgModule} from '@angular/core';
import {HeaderComponent} from "./header/header";
import {SidebarComponent} from "./sidebar/sidebar";
import {IonicModule} from "ionic-angular";

@NgModule({
    declarations: [
        HeaderComponent,
        SidebarComponent
    ],
    imports: [IonicModule],
    exports: [
        HeaderComponent,
        SidebarComponent
    ]
})
export class ComponentsModule {
}
