import {Component} from "@angular/core";
import {ModalController} from "ionic-angular";
import {NotificationPage} from "../../pages/notification/notification";

@Component({
    selector: "header",
    templateUrl: "header.html"
})
export class HeaderComponent {

    constructor(public modal: ModalController) {

    }

    openModal() {
        this.modal.create(NotificationPage).present();
    }

}