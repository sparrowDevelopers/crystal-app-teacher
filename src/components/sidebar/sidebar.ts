import {Component} from '@angular/core';
import {NavController, AlertController, App} from "ionic-angular";
import {AboutPage} from "../../pages/about/about";
import {PrivacyPolicyPage} from "../../pages/privacy-policy/privacy-policy";
import {TermsConditionsPage} from "../../pages/terms-conditions/terms-conditions";
import {StorageService} from "../../services/storage.service";
import {ApiService} from "../../services/api.service";
import {AlertService} from "../../services/alert.service";
import {LoginPage} from "../../pages/login/login";

@Component({
    selector: 'sidebar',
    templateUrl: 'sidebar.html',
    providers: [ApiService, StorageService, AlertService]
})
export class SidebarComponent {

    about = AboutPage;
    privacy = PrivacyPolicyPage;
    terms = TermsConditionsPage;

    constructor(public navCtrl: NavController,
                public api: ApiService,
                public store: StorageService,
                public alert: AlertService,
                public alertCtrl: AlertController,
                public app: App) {

        console.log('Hello SidebarComponent Component');
    }

    openPage(page) {
        this.navCtrl.push(page);
    }

    logout() {

        this.alertCtrl.create({

            title: "Alert",
            subTitle: "Are you sure to logout?",
            buttons: [
                {
                    text: "Yes",
                    role: "cancel",
                    handler: () => {

                        this.store.get("userData").then(data => {
                            this.api.get("deleteToken.php?id=" + data["token"]).subscribe(res => {
                                this.store.clear().then(() => {
                                    this.app.getRootNav().setRoot(LoginPage);
                                });
                            }, () => {
                                this.alert.show();
                            });
                        });

                    }
                },
                {
                    text: "No",
                    role: "cancel"
                }
            ]

        }).present();

    }

}
