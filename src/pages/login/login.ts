import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, AlertController, LoadingController} from 'ionic-angular';
import {ApiService} from '../../services/api.service';
import {StorageService} from '../../services/storage.service';
import {Push, PushObject, PushOptions} from "@ionic-native/push";
import {TabsPage} from "../tabs/tabs";

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
    providers: [ApiService, StorageService, Push]
})
export class LoginPage {

    public username: string;
    public password: string;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public api: ApiService,
                public store: StorageService,
                public loading: LoadingController,
                public alert: AlertController,
                public push: Push) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad LoginPage');
    }

    onSubmit() {

        if (this.username !== undefined && this.password !== undefined && this.username !== "" && this.password !== "") {

            let obj = {
                username: this.username,
                password: this.password
            };

            console.log(obj);

            let loader = this.loading.create({content: "Logging in..."});
            loader.present();

            this.api.post("login.php", obj).subscribe((data) => {

                console.log(data);
                loader.dismiss();

                if (data["capability"] === "teacher") {


                    this.push.hasPermission().then((res) => {

                        if (res.isEnabled) {

                            const options: PushOptions = {
                                android: {
                                    senderID: "261348170975"
                                },
                                ios: {
                                    alert: true,
                                    sound: true
                                }
                            };


                            const pushObject: PushObject = this.push.init(options);

                            pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));

                            pushObject.on('registration').subscribe((registration: any) => {

                                let obj = {
                                    token: registration["registrationId"],
                                    userid: data["userID"],
                                    type: 1
                                };

                                this.api.post("saveToken.php", obj).subscribe(res => {
                                    data["token"] = res["token"]["id"];
                                    this.store.set("userData", data).then(() => {
                                        this.navCtrl.setRoot(TabsPage);
                                    }).catch(() => {
                                        console.log("Error in storing user information");
                                    });

                                });
                            });

                        } else {
                            this.alert.create({
                                subTitle: "Device not support notifications",
                                buttons: [
                                    {
                                        text: "Ok",
                                        role: "cancel"
                                    }
                                ]
                            }).present();
                        }

                    });

                } else {
                    this.alert.create({
                        subTitle: "Invalid login credentials",
                        buttons: [
                            {
                                text: "Ok",
                                role: "cancel",
                                handler: value => {
                                    this.username = this.password = "";
                                }
                            }
                        ]
                    }).present();
                }


            }, (error) => {
                loader.dismiss();
                console.log(error);
                this.alert.create({
                    subTitle: "Invalid login credentials",
                    buttons: [
                        {
                            text: "Ok",
                            role: "cancel"
                        }
                    ]
                }).present();
            });

        } else {

            this.alert.create({
                subTitle: "Please fill all fields",
                buttons: [
                    {
                        text: "Ok",
                        role: "cancel"
                    }
                ]
            }).present();

        }

    }

}

