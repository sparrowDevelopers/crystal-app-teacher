import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {HomePage} from "../home/home";
import {CalenderPage} from "../calender/calender";
import {CircularPage} from "../circular/circular";
import {SubjectsPage} from "../subjects/subjects";

@IonicPage()
@Component({
    selector: 'page-tabs',
    templateUrl: 'tabs.html',
})
export class TabsPage {

    tab1 = HomePage;
    tab2 = SubjectsPage;
    tab3 = CalenderPage;
    tab4 = CircularPage;

    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad TabsPage');
    }


}
