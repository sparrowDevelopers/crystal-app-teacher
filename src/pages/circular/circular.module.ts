import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {CircularPage} from './circular';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
    declarations: [
        CircularPage
    ],
    imports: [
        IonicPageModule.forChild(CircularPage),
        ComponentsModule
    ],
})
export class CircularPageModule {
}
