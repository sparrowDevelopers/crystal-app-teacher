import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import {ApiService} from "../../services/api.service";
import {AlertService} from "../../services/alert.service";

@IonicPage()
@Component({
    selector: 'page-circular',
    templateUrl: 'circular.html',
    providers: [ApiService, AlertService]
})
export class CircularPage {

    public circular = [];
    public monthname = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public api: ApiService,
                public loading: LoadingController,
                public alert: AlertService) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad CircularPage');
    }

    getDate(date) {
        let d = new Date(date);
        return d.getDate() + " " + this.monthname[d.getMonth()] + " " + d.getFullYear();
    }

    ionViewCanEnter() {
        return new Promise<any>((resolve) => {

            let loader = this.loading.create({content: "Retriving data..."});
            loader.present();

            this.api.get("circulars.php?id=teacher").subscribe(data => {
                this.circular = data;
                loader.dismiss().then(() => resolve(true));
            }, () => {
                loader.dismiss(() => resolve(true));
                this.alert.show();
            });

        });

    }

}
