import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {SubjectNotesPage} from './subject-notes';

@NgModule({
    declarations: [
        SubjectNotesPage,
    ],
    imports: [
        IonicPageModule.forChild(SubjectNotesPage),
    ],
})
export class SubjectNotesPageModule {
}
