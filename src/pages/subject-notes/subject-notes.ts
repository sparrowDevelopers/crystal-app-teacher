import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {FilePath} from "@ionic-native/file-path";
import {FileChooser} from "@ionic-native/file-chooser";
import {FileTransfer, FileUploadOptions, FileTransferObject} from "@ionic-native/file-transfer";
import {ApiService} from "../../services/api.service";
import {AlertService} from "../../services/alert.service";

@IonicPage()
@Component({
    selector: 'page-subject-notes',
    templateUrl: 'subject-notes.html',
    providers: [FilePath, FileChooser, FileTransfer, ApiService, AlertService]
})
export class SubjectNotesPage {

    public note;
    public tid = -1;
    public std;
    public div;
    public subId;
    public uploadId: string = "";
    public filename: string = "";

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public fileChooser: FileChooser,
                public filePath: FilePath,
                public transfer: FileTransfer,
                public api: ApiService,
                public viewCtrl: ViewController,
                public alert: AlertService) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SubjectNotesPage');
    }

    ionViewWillLoad() {
        this.tid = this.navParams.get("userid");
        this.std = this.navParams.get("std");
        this.div = this.navParams.get("div");
        this.subId = this.navParams.get("subId");
    }

    onSubmitNote(form) {
        if (this.uploadId === "") {
            this.alert.show("Upload file first", () => {
            });
        } else if (this.note === "" || this.note === undefined) {
            alert("Write title for note");
        } else {
            let data = {
                subId: this.subId,
                div: this.div,
                std: this.std,
                notes: this.note,
                userid: this.tid,
                file: this.uploadId
            };

            this.api.post("addNotes.php", data).subscribe(() => {
                this.alert.show("Notes uploaded successfully", () => {
                    this.viewCtrl.dismiss();
                });
            }, () => {
                this.alert.show();
            });

        }

    }


    uploadFile() {


        this.fileChooser.open().then((uri) => {

            this.filePath.resolveNativePath(uri).then(path => {


                let array = path.split("/");
                let filename = array[array.length - 1];
                this.filename = filename;

                const fileTransfer: FileTransferObject = this.transfer.create();
                let options: FileUploadOptions = {
                    fileKey: "file",
                    fileName: filename
                };

                fileTransfer.upload(path, this.api.getApi() + "uploadFile.php", options).then((upload) => {
                    this.uploadId = upload["response"].replace(/[^\d]/g, '');
                }).catch(() => {
                    this.alert.show("Error in file uploading", () => {
                    });
                });


            }).catch(() => {
                this.alert.show("Error in resolving file path", () => {
                });
            })

        }).catch(() => {
            this.alert.show("File chooser not working", () => {
            });
        });


    }

}
