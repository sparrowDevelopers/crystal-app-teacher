import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import {ApiService} from "../../services/api.service";
import {AlertService} from "../../services/alert.service";

@IonicPage()
@Component({
    selector: 'page-notification',
    templateUrl: 'notification.html',
    providers: [ApiService, AlertService]
})
export class NotificationPage {

    public notifications = [];


    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public alert: AlertService,
                public api: ApiService,
                public loading: LoadingController) {
    }

    ionViewCanEnter() {
        return new Promise<any>((resolve) => {

            let loader = this.loading.create({content: "Retriving data..."});
            loader.present();

            this.api.get("getNotifications.php?id=teacher").subscribe((data) => {
                this.notifications = data;
                loader.dismiss().then(() => resolve(true));
            }, () => {
                loader.dismiss().then(() => resolve(true));
                this.alert.show();
            });

        });
    }

    openNotification(item) {
        this.alert.show(item["body"], () => {
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad NotificationPage');
    }

}
