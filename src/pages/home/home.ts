import {Component, OnInit} from '@angular/core';
import {NavController, LoadingController, ModalController} from 'ionic-angular';
import {ApiService} from "../../services/api.service";
import {StorageService} from "../../services/storage.service";
import {ClassModalPage} from "../class-modal/class-modal";
import {forkJoin} from "rxjs/observable/forkJoin";
import {AlertService} from "../../services/alert.service";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
    providers: [ApiService, StorageService, AlertService]
})
export class HomePage implements OnInit {

    public monthnames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Nov", "Dec"];
    public day;
    public month;
    public year;
    public classes = [];
    public isClassTeacher = false;
    public circular = [];
    public calendar = [];
    public monthname = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

    constructor(public navCtrl: NavController,
                public api: ApiService,
                public store: StorageService,
                public loading: LoadingController,
                public modal: ModalController,
                public alert: AlertService) {

    }

    ngOnInit() {
        let date = new Date();
        this.day = date.getDate();
        this.month = this.monthnames[date.getMonth()];
        this.year = date.getFullYear();
    }

    getDate(date) {
        let d = new Date(date);
        return d.getDate() + " " + this.monthnames[d.getMonth()] + " " + d.getFullYear();
    }

    openClassModal() {
        this.modal.create(ClassModalPage, this.classes).present();
    }


    ionViewCanEnter() {

        return new Promise<any>((resolve) => {

            let loader = this.loading.create({
                content: "Retriving data..."
            });

            loader.present();

            this.store.get("userData").then(userData => {

                forkJoin([
                    this.api.get("checkClassTeacher.php?id=" + userData["ID"]),
                    this.api.get("circulateLatest.php?id=teacher"),
                    this.api.get("calenderRecent.php")
                ]).subscribe(([classes, circulars, calendar]) => {

                    this.circular = circulars;
                    this.calendar = calendar;
                    if (classes.length !== 0) {
                        this.classes = classes;
                        this.isClassTeacher = true;
                    }
                    loader.dismiss().then(() => {
                        resolve(true);
                    });

                }, () => {
                    this.alert.show();
                });


            });

        });

    }

}
