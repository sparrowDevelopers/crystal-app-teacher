import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import {ApiService} from "../../services/api.service";
import {AlertService} from "../../services/alert.service";

@IonicPage()
@Component({
    selector: 'page-calender',
    templateUrl: 'calender.html',
    providers: [ApiService, AlertService]
})
export class CalenderPage {

    public calendar = [];
    public monthname = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];


    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public loading: LoadingController,
                public api: ApiService,
                public alert: AlertService) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad CalenderPage');
    }

    getDate(date) {
        let d = new Date(date);
        return d.getDate() + " " + this.monthname[d.getMonth()] + " " + d.getFullYear();
    }

    ionViewCanEnter() {
        return new Promise<any>((resolve) => {

            let loader = this.loading.create({content: "Retriving data..."});
            loader.present();

            this.api.get("calender.php").subscribe(data => {

                this.calendar = data;

                loader.dismiss().then(() => {
                    resolve(true);
                });
            }, () => {
                loader.dismiss(() => resolve(true));
                this.alert.show();
            });

        });
    }

}
