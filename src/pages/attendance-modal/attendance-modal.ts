import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {ApiService} from "../../services/api.service";
import {StorageService} from "../../services/storage.service";
import {AlertService} from "../../services/alert.service";


@IonicPage()
@Component({
    selector: 'page-attendance-modal',
    templateUrl: 'attendance-modal.html',
    providers: [ApiService, StorageService, AlertService]
})
export class AttendanceModalPage {

    public attended = [];
    public class_;
    public present;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public api: ApiService,
                public store: StorageService,
                public alert: AlertService,
                public viewCtrl: ViewController) {

    }

    ionViewWillLoad() {
        this.attended = this.navParams.get("data");
        this.class_ = this.navParams.get("class_");
        this.present = this.navParams.get("present");
    }

    updateAttendance() {
        let result = this.attended.filter(x => x.checked).map(x => x.id);

        this.store.get("userData").then(userData => {
            this.api.post("updateAttendance.php?id=" + this.class_, {
                student: result,
                tid: userData["ID"]
            }).subscribe(() => {
                this.alert.show("Attendance updated successfully", () => {
                    this.viewCtrl.dismiss();
                });
            }, () => {
                this.alert.show();
            });
        });


    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad AttendanceModalPage');
    }

}
