import {LoginPageModule} from "./login/login.module";
import {HomeModule} from "./home/home.module";
import {CircularPageModule} from "./circular/circular.module";
import {CalenderPageModule} from "./calender/calender.module";
import {NotificationPageModule} from "./notification/notification.module";
import {SubjectNotesPageModule} from "./subject-notes/subject-notes.module";
import {SubjectsPageModule} from "./subjects/subjects.module";
import {ErrorPageModule} from "./error/error.module";
import {TabsPageModule} from "./tabs/tabs.module";
import {AttendanceModalPageModule} from "./attendance-modal/attendance-modal.module";
import {ClassModalPageModule} from "./class-modal/class-modal.module";
import {AboutPageModule} from "./about/about.module";
import {PrivacyPolicyPageModule} from "./privacy-policy/privacy-policy.module";
import {TermsConditionsPageModule} from "./terms-conditions/terms-conditions.module";

import {NgModule} from "@angular/core";

@NgModule({
    imports: [
        LoginPageModule,
        HomeModule,
        CircularPageModule,
        CalenderPageModule,
        NotificationPageModule,
        SubjectNotesPageModule,
        SubjectsPageModule,
        ErrorPageModule,
        TabsPageModule,
        AttendanceModalPageModule,
        ClassModalPageModule,
        AboutPageModule,
        PrivacyPolicyPageModule,
        TermsConditionsPageModule
    ],
    exports: [
        LoginPageModule,
        HomeModule,
        CircularPageModule,
        CalenderPageModule,
        NotificationPageModule,
        SubjectNotesPageModule,
        SubjectsPageModule,
        ErrorPageModule,
        TabsPageModule,
        AttendanceModalPageModule,
        ClassModalPageModule,
        AboutPageModule,
        PrivacyPolicyPageModule,
        TermsConditionsPageModule
    ]

})
export class PagesModule {
}