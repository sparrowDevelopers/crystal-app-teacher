import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController, ModalController} from 'ionic-angular';
import {ApiService} from "../../services/api.service";
import {StorageService} from "../../services/storage.service";
import {SubjectNotesPage} from "../subject-notes/subject-notes";
import {AlertService} from "../../services/alert.service";

@IonicPage()
@Component({
    selector: 'page-subjects',
    templateUrl: 'subjects.html',
    providers: [ApiService, StorageService, AlertService]
})
export class SubjectsPage {


    public subjects = [];
    public tid = -1;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public loading: LoadingController,
                public api: ApiService,
                public store: StorageService,
                public modal: ModalController,
                public alert: AlertService) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SubjectsPage');
    }

    openNotesModal(item) {
        let data = {
            std: item["std"],
            div: item["division"],
            userid: this.tid,
            subId: item["subid"]

        };
        this.modal.create(SubjectNotesPage, data).present();
    }

    ionViewCanEnter() {
        return new Promise<any>((resolve) => {

            let loader = this.loading.create({
                content: "Retriving subjects..."
            });
            loader.present();

            this.store.get("userData").then(userData => {
                this.tid = userData["ID"];

                this.api.get("subjectsTeaching.php?id=" + this.tid).subscribe(data => {
                    this.subjects = data;
                    loader.dismiss().then(() => resolve(true));
                }, () => {
                    loader.dismiss().then(() => resolve(true));
                    this.alert.show();
                });

            });
        });
    }

}
