import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {SubjectsPage} from './subjects';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
    declarations: [
        SubjectsPage
    ],
    imports: [
        IonicPageModule.forChild(SubjectsPage),
        ComponentsModule
    ],
})
export class SubjectsPageModule {
}
