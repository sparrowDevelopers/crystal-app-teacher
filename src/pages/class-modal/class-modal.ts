import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController, ModalController} from 'ionic-angular';
import {ApiService} from "../../services/api.service";
import {AttendanceModalPage} from "../attendance-modal/attendance-modal";
import {AlertService} from "../../services/alert.service";

@IonicPage()
@Component({
    selector: 'page-class-modal',
    templateUrl: 'class-modal.html',
    providers: [ApiService, AlertService]
})
export class ClassModalPage {

    public classes = [];
    public students = {};

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public api: ApiService,
                public loading: LoadingController,
                public modal: ModalController,
                public alert: AlertService) {
    }

    ionViewWillLoad() {
        this.classes = this.navParams.data;
    }

    updateAttendance(item) {

        this.api.get("getStudents.php?id=" + item["id"]).subscribe(data => {
            this.modal.create(AttendanceModalPage, {
                data: data["result"],
                class_: item["id"],
                present: data["present"]
            }).present();
        }, () => {
            this.alert.show();
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ClassModalPage');
    }

}
